import 'package:flutter/material.dart';

class TestPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Test Page'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.emoji_emotions_outlined),
        onPressed: () {
          _mostrarAlert(context);
        },
      ),
    );
  }

  void _mostrarAlert(BuildContext context) {
    showDialog(
      context: context, 
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          title: Text('es una alerta'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              FadeInImage(
                image: NetworkImage('https://http2.mlstatic.com/D_NQ_NP_956509-CBT47058511961_082021-O.webp', scale: .5),
                placeholder: AssetImage('assets/homero.gif'),
                fadeInDuration: Duration(milliseconds: 500),
              )
            ],
          ),
          actions: <Widget>[
            RawMaterialButton(
              child: Icon(Icons.access_alarms),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop();
              },
            )
          ],
        );
      }
    );
  }
}