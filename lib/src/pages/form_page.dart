import 'package:ex3rutas/src/utils/validators_util.dart';
import 'package:flutter/material.dart';

class FormPage extends StatefulWidget {
  @override
  _FormPageState createState() => _FormPageState();
}

class _FormPageState extends State<FormPage> {
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 40),
        child: Form(
          key: formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 10),
              Text("Formulario", style: TextStyle(fontSize: 30, color: Colors.black),),
              Text("Llena los datos", style: TextStyle(fontSize: 25, color: Colors.black)),
              SizedBox(height: 30),
              TextFormField(
                decoration: InputDecoration(
                  labelText: "Ingresa tu nombre"
                ),
                validator: validarNombre,
              ),
              SizedBox(height: 30),
              TextFormField(
                decoration: InputDecoration(
                  labelText: "Ingresa tu telefono"
                ),
                validator: validarTelefono,
              ),
              SizedBox(height: 30),
              TextFormField(
                decoration: InputDecoration(
                  labelText: "Ingresa tu email",
                ),
                validator: validarEmail,
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.login),
        onPressed: () {
          if (formKey.currentState!.validate()) {
            print('todo bien');
          }
        },
      ),
    );
  }
}