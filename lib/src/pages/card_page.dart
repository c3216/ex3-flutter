import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Card Page'),
      ),
      body: ListView(
        children: _renderCards(context),
      ),
    );
  }

  List<Widget> _renderCards(BuildContext context) {
    return <Widget>[
      Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        child: Column(
          children: <Widget>[_cardt1(), SizedBox(height: 10), _cardt2(context)],
        ),
      )
    ];
  }

  Widget _cardt1() {
    return Card(
      color: Colors.amber,
      child: Column(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.add_alarm, color: Colors.cyanAccent),
            title: Text('Daniel Lozano'),
            subtitle: Text(
                'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
          ),
          Row(
            children: <Widget>[
              RawMaterialButton(
                child: Text('Aceptar'),
                onPressed: () {},
              ),
              RawMaterialButton(
                child: Text('Cancelar'),
                onPressed: () {},
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _cardt2(BuildContext context) {
    final _card = Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: <Widget>[
          FadeInImage(
            image: NetworkImage('https://cdn1.epicgames.com/undefined/offer/batman-arkham-asylum_promo-1567x917-f1248a17909e224f91845b223542306a.jpg'),
            placeholder: AssetImage('assets/upload.gif'),
            fadeInDuration: Duration(milliseconds: 500),
            height: 200.0,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.cover,

          ),
          Container(
            padding: EdgeInsets.all(5.0),
            child: Text('Esta es una imagen'),
          )
        ],
      ),
    );

    return Container(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: _card,
      ),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black38,
                blurRadius: 10,
                spreadRadius: 0.2,
                offset: Offset(0.0, 5.0))
          ],
          color: Colors.white),
    );
  }
}
