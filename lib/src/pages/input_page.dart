import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

class InputPage extends StatefulWidget {
  @override
  _InputPageState createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  String _nombre = '';
  String _email = '';
  String _password = '';
  String _fecha = '';


  TextEditingController _inputFieldDateController = TextEditingController();

  List<String> _perros = [
    'Husky',
    'Chihuahua',
    'Doberman',
    'Labrador',
    'Terry',
    'Boxer'
  ];
 
  String _optionPreselect = 'Husky';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Input Page'),
      ),
      body: ListView(
        padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
        children: <Widget>[
          _crearInput(),
          Divider(),
          _crearEmail(),
          Divider(),
          _crearPassword(),
          Divider(),
          _crearFecha(context),
          Divider(),
          _crearLista()
        ],
      ),
    );
  }

  Widget _crearInput() {
    return TextField(
      textCapitalization: TextCapitalization.words,
      decoration: InputDecoration(
          counter: Text('caracteres ${_nombre.length}'),
          hintText: 'Nombre Completo',
          labelText: 'nombre',
          helperText: 'solo letras',
          icon: Icon(Icons.person),
          suffixIcon: Icon(Icons.ac_unit),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10))),
      onChanged: (valor) {
        setState(() {
          _nombre = valor;
        });
      },
    );
  }

  Widget _crearEmail() {
    return TextField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          hintText: 'email',
          labelText: 'email',
          suffixIcon: Icon(Icons.alternate_email),
          icon: Icon(Icons.email),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
      onChanged: (value) => setState(() {
        _email = value;
      }),
    );
  }

  Widget _crearPassword() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
          hintText: 'password',
          labelText: 'password',
          suffixIcon: Icon(Icons.lock_open),
          icon: Icon(Icons.lock),
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
      onChanged: (value) => setState(() {
        _password = value;
      }),
    );
  }

  Widget _crearFecha(BuildContext context) {
    return TextField(
      controller: _inputFieldDateController,
      readOnly: true,
      enableInteractiveSelection: false,
      decoration: InputDecoration(
        hintText: 'Fecha de nacimiento',
        labelText: 'Fecha',
        suffixIcon: Icon(Icons.perm_contact_calendar),
        icon: Icon(Icons.calendar_today),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10))
      ),
      onTap: () {
        //FocusScope.of(context).requestFocus(new FocusNode());
        _selectDate(context);
      },
    );
  }

  void _selectDate(BuildContext context) async {
    DateTime? picker = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime(2018),
      lastDate: new DateTime(2030)
    );

    if(picker != null) {
      _fecha = DateFormat("dd/MM/yyyy").format(picker).toString();
      _inputFieldDateController.text = _fecha;
    }
  }

  Widget _crearLista() {
    return Row(
      children: <Widget>[
        Icon(Icons.select_all),
        SizedBox(width: 30),
        Expanded(
          child: DropdownButton(
            value: _optionPreselect,
            items: _getOpciones(),
            onChanged: (opt) {
              setState(() {
                _optionPreselect = opt.toString();
              });
            },
          ),
        )
      ],
    );
  }

  List<DropdownMenuItem<String>> _getOpciones() {
    List<DropdownMenuItem<String>> lista = [];
    _perros.forEach((perro) {
      lista.add(DropdownMenuItem(
        value: perro,
        child: Text(perro),
      ));
    });
    return lista;
  }
}
