


import 'package:ex3rutas/src/pages/animals_detail_page.dart';
import 'package:ex3rutas/src/pages/animals_list_page.dart';
import 'package:ex3rutas/src/pages/animated_container_page.dart';
import 'package:ex3rutas/src/pages/card_page.dart';
import 'package:ex3rutas/src/pages/form_page.dart';
import 'package:ex3rutas/src/pages/home_page.dart';
import 'package:ex3rutas/src/pages/input_page.dart';
import 'package:ex3rutas/src/pages/listview_page.dart';
import 'package:ex3rutas/src/pages/test_page.dart';
import 'package:flutter/cupertino.dart';

Map<String, WidgetBuilder> getAplicationRoutes() {
  return <String, WidgetBuilder>{
    '/' : (BuildContext context) => HomePage(),
    'test' : (BuildContext context) => TestPage(),
    'inputs' : (BuildContext context) => InputPage(),
    'cards' : (BuildContext context) => CardPage(),
    'list' : (BuildContext context) => ListPage(),
    'animatedContainer' : (BuildContext context) => AnimatedContainerPage(),
    'forms' : (BuildContext context) => FormPage(),
    'animalsList' : (BuildContext context) => AnimalsListPage(),
    'animalsDetail' : (BuildContext context) => AnimalDetailPage()
  };
}